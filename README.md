# Real World Time
Real World Time is a Minetest mod that syncs in-game time with the real-world time, so that the time in the game will always be the same as the time in real life. As a result, the day-night cycle will sync with the real-world one and will cycle at the same speed as the real world.

## How it works
If enabled, this mod syncs the time of your world with your system clock and adjusts the game's time-speed to be the same as the real-world. Then, it verifies every 60 seconds to make sure the time is properly synced.

## Commands
To manually sync the time with the real-world time, run:
```
/sync_time
```

To stop syncing and restore the default time speed, run:
```
/unsync_time
```
> The `server` priv is required to run the commands.

## Disabling the Mod
To disable this mod and not sync the time anymore, run `/unsync_time`, close the world, and disable this Mod.